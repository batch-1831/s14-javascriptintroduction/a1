// console.log("Hello world");

let firstName = "John", lastName = "Smith";
let age = 30;
let hobbies = ["Biking", "Mountain Climbing", "Swimming"];
let workAddress = {
    houseMunber: "32",
    street: "Washington",
    city: "Lincoln",
    state: "Nebraska"
}
let isMarried = true;

function printUserInfo(firstName, lastName, age, hobbies, workAddress) {
    console.log("First Name: " + firstName);
    console.log("Last Name: " + lastName);
    console.log("Age: " + age);
    console.log("Hobbies:");
    console.log(hobbies);
    console.log("Work Address:");
    console.log(workAddress);
}

printUserInfo(firstName, lastName, age, hobbies, workAddress);

function returnFunction(firstName, lastName, age) {
    return firstName + ' ' + lastName + ' ' + age + ' ' + hobbies + ' ' + workAddress;
}

let completeNameAge = returnFunction(firstName, lastName, age);
console.log(firstName + ' '+ lastName + ' is ' + age + ' years of age.');

console.log("This was printed inside of the function")
console.log(hobbies);

console.log("This was printed inside of the function")
console.log(workAddress);

console.log("The value of isMarried is: " + isMarried);
